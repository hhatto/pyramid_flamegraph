all:
	@echo "make [check|pypireg]"

check:
	python setup.py check -r

pypireg:
	rm -rf *.egg-info/* dist/*
	pip install twine wheel
	python setup.py register
	python setup.py sdist bdist_wheel
	twine upload dist/*
